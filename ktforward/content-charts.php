<?php
/**
 * @package Forward
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php ktforward_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	</div>

	<div class="container">
        <div class="entry-content">
            <?php the_content(); ?>
            
            <?php
				// Find connected pages
				$connected = new WP_Query( array(
				  'connected_type' => 'songs_to_charts',
				  'connected_items' => get_queried_object(),
				  'nopaging' => true,
				  'connected_orderby' => 'position',
				  'connected_order' => 'asc',
				  'connected_order_num' => 'true',
				) );
				
				// Display connected pages
				if ( $connected->have_posts() ) :
				?>
                    <div class="chart-legend chart-entry">
                        <div class="chart-ranks">
                            <span class="chart-lastweek">
                            </span>
                            
                            <span class="chart-legend-thisweek">
                                <h3>TW</h3>
                            </span>
                        </div>
                        
                        <div class="chart-legend-thumbnail">
                            <h3>Artwork</h3>
                        </div>
                        
                        <div class="chart-legend-details">
                                    <h3>Song Details</h3>
                       </div>
                       <div class="chart-legend-links">
						  <h3>Buy</h3>
                       </div>
                    </div>
				<div class="chart-list">
					<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
                    <?php $chart_entry = p2p_get_meta( get_post()->p2p_id, 'entry', true ); ?>
                    <?php if($chart_entry == "Debut"){ ?>
                        <div class="chart-entry debut">
                    <?php } elseif($chart_entry == "Re-Entry"){ ?>
                        <div class="chart-entry re-entry">                    
                    <?php } else { ?>                  
                        <div class="chart-entry">
                    <?php } ?>
                        <div class="chart-ranks">
                            <span class="chart-lastweek">
                            </span>
                            
                            <span class="chart-thisweek">
                                <?php
                                echo p2p_get_meta( get_post()->p2p_id, 'position', true );
                                 ?>
                            </span>
                    <?php $chart_entry = p2p_get_meta( get_post()->p2p_id, 'entry', true ); ?>
                    <?php if($chart_entry == "Debut"){ ?>
                    	<span class="chart-msg-debut">Debut</span>
                    <?php } elseif($chart_entry == "Re-Entry"){ ?>
                    	<span class="chart-msg-re-entry">Re-Entry</span>                   
                    <?php } else {} ?>                  
                        </div>
                        
                        <div class="chart-song-thumbnail">
                            <?php the_post_thumbnail( 'medium' ); ?>
                        </div>
                        
                        <div class="chart-song-details">
                                <span class="chart-song-title">
                                    <h2><a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a></h2>
                                </span>
                                <span class="chart-song-artist">
                                    <h3><?php the_terms( $post->ID, 'artist', ' ', ', ', ' ' ); ?></h3>
                                </span>
                            <span class="chart-album-title">
							<h4><?php $_my_meta_value_key = get_post_meta($post->ID, '_my_meta_value_key', true ); 
                            if($_my_meta_value_key != ""){
                            ?>
                            <?php echo $_my_meta_value_key; ?>
                            <?php } ?></h4>
                            </span>
                            <span class="chart-song-label">
                            <h4><?php the_terms( $post->ID, 'record-label', ' ', '/', ' ' ); ?></h4>
                            </span>
                       </div>
                       <div class="chart-song-links">
						  <?php if(get_field('itunes')) : ?>
                                <a href="<?php the_field('itunes'); ?>" title="Buy <?php the_title(); ?> on iTunes" /><div class="link-itunes"></div></a>
                          <?php endif; ?>
                                <a href="http://www.amazon.com/gp/search?ie=UTF8&camp=1789&creative=9325&index=digital-music&keywords=<?php echo strip_tags( get_the_term_list($post->ID, 'artist') ); ?> <?php the_title(); ?>&linkCode=ur2&tag=thevoiintra0a-20&linkId=ZV7OK35DQOQ5TTNE" title="Buy <?php the_title(); ?> on Amazon"><div class="link-amazon"></div></a>
                       </div>
                    </div>
                    <?php endwhile; ?>
				</div>
				
				<?php 
				// Prevent weirdness
				wp_reset_postdata();
				
				endif;
			?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'ktforward' ),
                    'after'  => '</div>',
                ) );
            ?> 
        </div><!-- .entry-content -->
    </div><!-- .container -->

            
	<div class="container">
        <footer class="entry-footer">
            <?php ktforward_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
