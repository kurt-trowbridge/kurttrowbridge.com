<?php
/**
 * @package Forward
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
		<div class="entry-meta">
			<?php ktforward_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	</div>

	<div class="container">
        <div class="entry-content">
            <?php the_content(); ?>

			<p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">View full chart <span class="meta-nav">→</span></a></p>

            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'ktforward' ),
                    'after'  => '</div>',
                ) );
            ?> 
        </div><!-- .entry-content -->
    </div><!-- .container -->

            
	<div class="container">
        <footer class="entry-footer">
            <?php ktforward_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
