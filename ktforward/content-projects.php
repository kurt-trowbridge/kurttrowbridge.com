<?php
/**
 * @package Forward
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php ktforward_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	</div>
    
	<div class="container">
        <div class="entry-content">
                  <div class="project-meta">
                    <?php if ( has_post_thumbnail() ) {                   
                      $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
                      echo '<img src="' . $image_src[0]  . '" width="100%" />';
                      } ?>
                    <div class="project-links">
                      <?php if(get_field('website_url')) : ?>
                        	<a href="<?php the_field('website_url'); ?>" title="View website for <?php the_title(); ?>" /><div class="link-website_url">View website</div></a>
					  <?php endif; ?>
                    </div>
                  </div>		
            <?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'ktforward' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div><!-- .entry-content -->
    </div><!-- .container -->

	<div class="container">
        <footer class="entry-footer">
            <?php ktforward_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
