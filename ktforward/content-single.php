<?php
/**
 * @package Forward
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php ktforward_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	</div>
    
	<div class="container">
        <div class="entry-content">
        	<?php if ( is_home()) : ?>
			<?php the_content( 'More' ); ?>
            <?php else : ?>
            <?php the_content('More'); ?>
            <?php endif; ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'ktforward' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div><!-- .entry-content -->
    </div><!-- .container -->

	<div class="container">
        <footer class="entry-footer">
            <?php ktforward_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
