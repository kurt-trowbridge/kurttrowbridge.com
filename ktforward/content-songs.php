<?php
/**
 * @package Forward
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php ktforward_posted_on(); ?>
		</div><!-- .entry-meta -->
        <h2><?php the_terms( $post->ID, 'artist', ' ', ', ', ' ' ); ?></h2>
		<h3><?php $_my_meta_value_key = get_post_meta($post->ID, '_my_meta_value_key', true ); 
            if($_my_meta_value_key != ""){
            ?>
            <span class="from-the-album">From the album</span> <em class="song-album-title"><?php echo $_my_meta_value_key; ?></em>
            <?php } ?>
        </h3>
	</header><!-- .entry-header -->
    </div>

<!-- test
            <?php
			// Find connected pages
			$connected = new WP_Query( array(
			  'connected_type' => 'songs_to_charts',
			  'connected_items' => get_queried_object(),
			  'nopaging' => true,
			  'orderby' => 'date',
			  'order' => 'asc',
			) );
			$weeks = new WP_Query( array(
                            'connected_type'  => 'songs_to_charts',
                            'connected_items' => get_queried_object(),
                            'nopaging' => true,
            ) );
			
			// Display connected pages
			if ( $connected->have_posts() ) :
			?>
			<div>
                <ul class="chart-line-graph">
                <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
                    <li style="width:calc(100% / <?php echo $weeks->found_posts; ?>);"><span style="height:calc(101% - <?php echo p2p_get_meta( get_post()->p2p_id, 'position', true ); ?>%);"><?php echo p2p_get_meta( get_post()->p2p_id, 'position', true ); ?></span></li>
                <?php endwhile; ?>
                </ul>
                
                <?php 
                // Prevent weirdness
                wp_reset_postdata();
                
                endif;
                ?>
            </div>

 /test -->
<!-- test2
            <?php
			// Find connected pages
			$connected = new WP_Query( array(
			  'connected_type' => 'songs_to_charts',
			  'connected_items' => get_queried_object(),
			  'nopaging' => true,
			  'orderby' => 'date',
			  'order' => 'asc',
			) );
			$weeks = new WP_Query( array(
                            'connected_type'  => 'songs_to_charts',
                            'connected_items' => get_queried_object(),
                            'nopaging' => true,
            ) );
            $i = 0;
			
			// Display connected pages
			if ( $connected->have_posts() ) :
			?>
			<div>

                <svg width="100%" height="100%" viewbox="0 0 100 100" class="chart">
				<polygon
					fill="#ff3818"
					stroke="#ff3818"
					stroke-width="0"
					points="0,100 <?php while ( $connected->have_posts() ) : $connected->the_post(); ?><?php echo ((100 * $i++)/($weeks->found_posts - 1));?>,<?php echo p2p_get_meta( get_post()->p2p_id, 'position', true ); ?> <?php endwhile; ?> 100, 100"/>
				</svg>
                
                <?php 
                // Prevent weirdness
                wp_reset_postdata();
                
                endif;
                ?>
            </div>

 /test2 -->

	<div class="container">
        <div class="entry-content">
            <div class="song-meta">
            <div style="position: relative;">
					<?php if ( has_post_thumbnail() ) {                   
	                      $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
	                      echo '<img src="' . $image_src[0]  . '" width="100%" data-adaptive-background="1" data-ab-parent=".song-meta" />';
	                      } ?>

	<!-- test3 -->
	            <?php
				// Find connected pages
				$connected = new WP_Query( array(
				  'connected_type' => 'songs_to_charts',
				  'connected_items' => get_queried_object(),
				  'nopaging' => true,
				  'orderby' => 'date',
				  'order' => 'asc',
				) );
				$weeks = new WP_Query( array(
	                            'connected_type'  => 'songs_to_charts',
	                            'connected_items' => get_queried_object(),
	                            'nopaging' => true,
	            ) );
	            $i = 0;
				
				// Display connected pages
				if ( $connected->have_posts() ) :
				?>
				<div style="position: absolute; bottom: 0;">

	                <svg width="100%" height="100%" viewbox="0 0 100 100" class="chart">
					<polygon
						fill="rgba(0,0,0,0.5)"
						class="svg--graph-artwork"
						stroke-width="0"
						points="0,100 <?php while ( $connected->have_posts() ) : $connected->the_post(); ?><?php echo ((100 * $i++)/($weeks->found_posts - 1));?>,<?php echo p2p_get_meta( get_post()->p2p_id, 'position', true ); ?> <?php endwhile; ?> 100, 100"/>
					</svg>
	                
	                <?php 
	                // Prevent weirdness
	                wp_reset_postdata();
	                
	                endif;
	                ?>
	            </div>

	<!-- /test3 -->
	</div>                      
                    <div class="song-links">
                    <!--<a href="https://itunes.com/<?php echo sanitize_title_with_dashes(strip_tags( get_the_term_list($post->ID, 'artist') )); ?>/<?php the_title(); ?>&at=11l5G2" title="Buy <?php the_title(); ?> on iTunes" /><div class="link-itunes"></div></a>-->
                      <?php if(get_field('itunes')) : ?>
                        	<a href="<?php the_field('itunes'); ?>" title="Buy <?php the_title(); ?> on iTunes" /><div class="link-itunes"></div></a>
					  <?php endif; ?>
						<a href="http://www.amazon.com/gp/search?ie=UTF8&camp=1789&creative=9325&index=digital-music&keywords=<?php echo strip_tags( get_the_term_list($post->ID, 'artist') ); ?> <?php the_title(); ?>&linkCode=ur2&tag=thevoiintra0a-20&linkId=ZV7OK35DQOQ5TTNE" title="Buy <?php the_title(); ?> on Amazon"><div class="link-amazon"></div></a>
                    </div>
            </div>
			<?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'ktforward' ),
                    'after'  => '</div>',
                ) );
            ?>
            
            <div class="song-chart-data">
				<div class="song-stats">
                    <span class="song-weeks">
                        <span class="song-data-figure"><?php $connected = new WP_Query( array(
                            'connected_type'  => 'songs_to_charts',
                            'connected_items' => get_queried_object(),
                            'nopaging' => true,
                        ) );
                        
                        echo $connected->found_posts; ?>
                        </span>
                        <span class="song-chart-weeks">Weeks</span>
                    </span>

                    <span class="song-peak">            
                        <span class="song-data-figure">
                        <?php $connected = new WP_Query( array(
						  'connected_type' => 'songs_to_charts',
						  'connected_items' => get_queried_object(),
						  'nopaging' => true,
						  'orderby' => 'date',
						  'order' => 'asc',
						) );
						$weeks = new WP_Query( array(
			                            'connected_type'  => 'songs_to_charts',
			                            'connected_items' => get_queried_object(),
			                            'nopaging' => true,
			            ) );
			            $minihope = array();

						// Display connected pages
						if ( $connected->have_posts() ) :
			            ?>
		                <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
			                <?php $minihope[] = p2p_get_meta( get_post()->p2p_id, 'position', true ); ?>
		                <?php endwhile; ?>
		                <?php echo min($minihope); ?>
		                <?php 
		                // Prevent weirdness
		                wp_reset_postdata();
		                
		                endif;
		                ?>
		                </span>
                        <span class="song-chart-peak">Peak</span>
                    </span>
				</div>
                                            
            <?php
			// Find connected pages
			$connected = new WP_Query( array(
			  'connected_type' => 'songs_to_charts',
			  'connected_items' => get_queried_object(),
			  'nopaging' => true,
			  'orderby' => 'date',
			  'order' => 'asc',
			) );
			
			// Display connected pages
			if ( $connected->have_posts() ) :
			?>
			<div class="song-chart-history">
                <h3>Chart History:</h3>
                <ul class="chart-history-list">
                <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
                    <li>
						<span class="chart-history-pos">
							<?php echo p2p_get_meta( get_post()->p2p_id, 'position', true ); ?>
                        </span>
                        <a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
                        </a>
                    </li>
                <?php endwhile; ?>
                </ul>
                
                <?php 
                // Prevent weirdness
                wp_reset_postdata();
                
                endif;
                ?>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .container -->

	<div class="container">
        <footer class="entry-footer">
            <?php ktforward_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
