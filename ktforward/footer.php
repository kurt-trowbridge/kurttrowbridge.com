<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Forward
 */
?>

	</div><!-- #content -->
    <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="site-info">
                <div class="container">
                	<div class="footer-left">
						<?php
                         
                        $username = 'trombone13';
                        $scrobbler_url = "http://ws.audioscrobbler.com/2.0/user/" . $username . "/recenttracks?limit=1";
                         
                        $scrobbler_cache_file = 'scrobbler_' . $username . '_data.cache';
                         
                        if (file_exists($scrobbler_cache_file)) {
                          if (time() - filemtime($scrobbler_cache_file) > 180) {
                            // if the file was created more than 3 minutes ago then delete.
                            unlink($scrobbler_cache_file);
                          } else {
                            $scrobbler_url = realpath('./' . $scrobbler_cache_file);
                          }
                        }
                         
                        if ($scrobbler_xml = file_get_contents($scrobbler_url)) {
                                $scrobbler_data = simplexml_load_string($scrobbler_xml);
                         
                          if (!file_exists($scrobbler_cache_file)) {
                            file_put_contents($scrobbler_cache_file, $scrobbler_xml);
                          }
                         
                                foreach ($scrobbler_data->track as $track) {}
                                
                                echo '<div class="lastfm-box">';
                                    echo '<div class="lastfm-np">';
                                        echo '<a href="http://last.fm/user/trombone13"><div class="lastfm-icon"></div>';
                                        echo '<div class="lastfm-lp">Last played:</div></a>';
                                    echo '</div>';
                                    echo '<img src="' . $track->image[3] . '" class="lastfm-thumb" />';
                                    echo '<div class="lastfm-details">';
                                        echo '<div class="lastfm-title">' . $track->name . '</div>';
                                        echo '<div class="lastfm-artist">' . $track->artist . '</div>';
                                    echo '</div>';
                                echo '</div>';
                        }
                        ?>
                    </div>
                    
                    <div class="footer-right">
                        <div class="footericons">
                            <a href="http://twitter.com/KurtTrowbridge" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter-48.png" alt="twitter" /></a>
                            <a href="http://facebook.com/KurtTrowbridge" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-48.png" alt="facebook" /></a>
                            <a href="http://linkedin.com/in/KurtTrowbridge" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin-48.png" alt="linkedin" /></a>
                        </div>
                            
                        <div class="footericons">
                            <a href="http://soundcloud.com/KurtTrowbridge" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/soundcloud-48.png" alt="soundcloud" /></a>
                            <a href="http://last.fm/user/trombone13" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/lastfm-48.png" alt="lastfm" /></a>
                            <a href="mailto:kurt@kurttrowbridge.com" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/email-48.png" alt="email" /></a>
                        </div>
                    </div>              
                
                    <div class="footer-copyright">
                        <span>&copy;<?php echo date("Y") ?> Kurt Trowbridge</span>
                        <span class="sep"> | </span>
                        <a href="#top">Back to top</a>
                    </div>
                </div>
            </div><!-- .site-info -->
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
