<?php
/**
 * Forward functions and definitions
 *
 * @package Forward
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'ktforward_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ktforward_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Forward, use a find and replace
	 * to change 'ktforward' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'ktforward', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'ktforward' ),
		'intro' => __( 'Intro Menu', 'ktforward' ),		
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ktforward_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // ktforward_setup
add_action( 'after_setup_theme', 'ktforward_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ktforward_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'ktforward' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'ktforward_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ktforward_scripts() {
	wp_enqueue_style( 'ktforward-style', get_stylesheet_uri() );

	wp_enqueue_script("jquery");

	wp_enqueue_script( 'ktforward-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'ktforward-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'ktforward-jquery.adaptive-backgrounds', get_template_directory_uri() . '/js/jquery.adaptive-backgrounds.js', array(), '', false );

	wp_enqueue_script( 'ktforward-jetpack-move-sharing', get_template_directory_uri() . '/js/jetpack-move-sharing.js', array(), '', true );

	wp_enqueue_script( 'ktforward-search-toggle', get_template_directory_uri() . '/js/search-toggle.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ktforward_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**** KURT ****/

/**
 * Enable thumbnails
 */
add_theme_support( 'post-thumbnails' ); 


/* 
*Adds the custom taxonomy function to our functions.php file. 
*/  
add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {  
    register_taxonomy(  'artist', 'post',   //Let WordPress know that the artist taxonomy has posts  
        array(  
            'hierarchical' => false,  
            'label' => 'Artists', // This tells WordPress how to label the various user interface outlets for the artist taxonomy      
            'query_var' => true,   
            'rewrite' => array( 'slug' => 'artist', 'with_front' => false ))  
            );
            
    register_taxonomy(  'record-label', 'post',   //Let WordPress know that the record label taxonomy has posts  
        array(  
            'hierarchical' => false,  
            'label' => 'Record Labels', // This tells WordPress how to label the various user interface outlets for the record label taxonomy      
            'query_var' => true,   
            'rewrite' => array( 'slug' => 'record-label', 'with_front' => false ))  
            );             
}  

add_theme_support( 'post-thumbnails', array( 'reviews', 'songs', 'projects' ) ); // Reviews


/* add column to edit-post columns */
function my_posts_columns($columns) {

	$new_columns = array(
		'artist' => __('Artists'),
		'record-label' => __('Record Labels'),
	);
    return array_merge($columns, $new_columns);
}
add_filter('manage_posts_columns' , 'my_posts_columns');


add_action( 'manage_posts_custom_column', 'my_manage_posts_columns', 10, 2 );

function my_manage_posts_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'artist' column. */
		case "artist":
			echo get_the_term_list($post->ID,'artist','',', ','');
      	break;

		case "record-label":
			echo get_the_term_list($post->ID,'record-label','',', ','');
      	break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}


/** add edit-post/quick-edit columns for album title, iTunes, Amazon for Songs **/
/* see file in Web Development folder */
/** end Songs additions **/


/* remove Author from edit-post columns */
function my_columns_filter( $columns ) {
   unset($columns['author']);
   return $columns;
   }
       
    // Filter pages
    add_filter( 'manage_edit-page_columns', 'my_columns_filter',10, 1 );	

    // Filter Posts
    add_filter( 'manage_edit-post_columns', 'my_columns_filter',10, 1 );

    // Custom Post Type
    add_filter( 'manage_edit-CUSTOMPOSTTYPE_columns', 'my_columns_filter',10, 1 );
	
/* remove Tags & Comments from Songs edit-post columns */
function my_columns_filter2( $columns ) {
   unset($columns['tags']);
   unset($columns['comments']);
   return $columns;
   }

    // Custom Post Type
    add_filter( 'manage_edit-songs_columns', 'my_columns_filter2',10, 1 );
	
/* Posts 2 Posts */
function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'songs_to_charts',
        'from' => 'songs',
        'to' => 'charts',
		
		'fields' => array(
			'position' => array(
					'type' => 'select',
					'title' => 'Position',
					'values' => range(1, 100)
				),
			'entry' => array( 
				'title' => 'Entry Type',
				'type' => 'select',
				'values' => array( 'Debut', 'Re-Entry' )
			),
		),
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );

/* add custom post types to tag/category search */
function add_custom_types_to_tax( $query ) {
if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

// Get all your post types
$post_types = get_post_types();

$query->set( 'post_type', $post_types );
return $query;
}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );

// sort tax

// function customize_customtaxonomy_archive_display ( $query ) {
//         if (($query->is_main_query()) && (is_tax('artist')))

//         $query->set( 'post_type', 'charts' );                 
//         $query->set( 'posts_per_page', '20' );
//         $query->set( 'orderby', 'date' );
//         $query->set( 'order', 'DESC' );
//     }

//      add_action( 'pre_get_posts', 'customize_customtaxonomy_archive_display' );