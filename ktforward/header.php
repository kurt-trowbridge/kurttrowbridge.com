<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Forward
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:image" content="http://kurttrowbridge.com/image.jpg" />
<?php if(is_singular( 'projects' )): ?>
    <title><?php wp_title( '| Projects |', true, 'right' ); ?></title>
<?php elseif(is_singular( 'charts' )): ?>
    <title><?php wp_title( '| Charts |', true, 'right' ); ?></title>
<?php elseif(is_singular( 'reviews' )): ?>
    <title><?php wp_title( '| Reviews |', true, 'right' ); ?></title>
<?php elseif(is_singular( 'songs' )): ?>
    <title><?php $artist_list = get_the_term_list( '', 'artist', __( '', 'ktforward' ), ', ' );
				$artist_list = strip_tags( $artist_list );
				printf( '' . __( '%1$s', 'ktforward' ) . ' - ', $artist_list );
				wp_title( '| Songs |', true, 'right' ); ?></title>
<?php else: ?>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
<?php endif; ?>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic|Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<a name="top"></a>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'ktforward' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
    	<div class="container">
            <div class="site-branding">
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
            </div>
    
            <nav id="site-navigation" class="main-navigation" role="navigation">
                <button class="menu-toggle"><?php _e( 'Primary Menu', 'ktforward' ); ?></button>
                <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            </nav><!-- #site-navigation -->
            
            <div class="header-social-media">
            	<a href="http://twitter.com/KurtTrowbridge" target="_blank">
                	<div class="sm-twitter"></div>	
                </a>
            	<a href="http://facebook.com/KurtTrowbridge" target="_blank">
                	<div class="sm-facebook"></div>	
                </a>
            	<a href="http://linkedin.com/in/KurtTrowbridge" target="_blank">
                	<div class="sm-linkedin"></div>	
                </a>
            	<a href="http://soundcloud.com/KurtTrowbridge" target="_blank">
                	<div class="sm-soundcloud"></div>	
                </a>
            	<a href="http://last.fm/user/trombone13" target="_blank">
                	<div class="sm-lastfm"></div>	
                </a>
            	<a href="mailto:kurt@kurttrowbridge.com" target="_blank">
                	<div class="sm-email"></div>	
                </a>                                     
            	<!--<a href="javascript:void(0)" id="search-click"><div class="search-toggle-nav">
                    <div class="search-toggle"></div>
                </div></a>-->
            </div>
        </div>
	</header><!-- #masthead -->

	<!--<div class="search-box" id="search-expand">
		<form role="search" method="get" class="search-form" action="<?php //echo home_url( '/' ); ?>">
            <label>
                <span class="screen-reader-text"><?php //echo _x( 'Search for:', 'label' ) ?></span>
                <div class="container search-container">
                <input type="search" class="search-field" placeholder="<?php //echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php //echo get_search_query() ?>" name="s" title="<?php //echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				</div>
            </label>
        </form>
    </div>-->
	<div id="content" class="site-content">
