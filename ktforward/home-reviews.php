<?php
/**
 * @package Forward
 */
?>

<?php if ( get_field( 'use_white_text' ) ): ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('review-white-text'); ?>>
<?php else: ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php endif; ?>
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title review-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
		<div class="entry-meta">
			<?php ktforward_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
              <div class="reviewmeta">
                <?php if ( has_post_thumbnail() ) {                   
                  $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
                  echo '<img src="' . $image_src[0]  . '" width="100%" data-adaptive-background="1" data-ab-parent=".container, .post-separator" />';
                  } ?>
                <?php if(get_field('release_date')) : ?><p class="meta releasedate">RELEASED: <?php the_field('release_date'); ?></p><?php endif; ?>
                <div class="reviewlinks">
                      <?php if(get_field('itunes')) : ?>
                        	<a href="<?php the_field('itunes'); ?>" title="Buy <?php the_title(); ?> on iTunes" /><div class="link-itunes"></div></a>
					  <?php endif; ?>
                      <?php if(get_field('amazon')) : ?>
                        	<a href="<?php the_field('amazon'); ?>" title="Buy <?php the_title(); ?> on Amazon"><div class="link-amazon"></div></a>
					  <?php endif; ?>
                </div>
              </div>		
		
		<?php the_excerpt(); ?>

	</div><!-- .entry-content -->

    </div><!-- .container -->

	<div class="container">
        <footer class="entry-footer">
            <?php ktforward_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>

</article><!-- #post-## -->
