<?php
/**
 * This is not displayed on the homepage, but is used for the smaller display on archive pages.
 *
 * @package Forward
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	                    <?php $chart_entry = p2p_get_meta( get_post()->p2p_id, 'entry', true ); ?>
                        <div class="chart-entry archive-chart-entry">
                        <div class="chart-ranks">
                         
                            <span class="song-weeks">
                                <span class="song-data-figure"><?php $connected = new WP_Query( array(
                                    'connected_type'  => 'songs_to_charts',
                                    'connected_items' => $post,
                                    'nopaging' => true,
                                ) );
                                
                                echo $connected->found_posts; ?>
                                </span>
                                <span class="song-chart-weeks">Weeks</span>
                            </span>
                            
                            <!--
                            <span class="song-peak">            
                            </span>
                            -->
                            
                        </div>
                        
                        <div class="chart-song-thumbnail">
                            <?php the_post_thumbnail( 'medium' ); ?>
                        </div>
                        
                        <div class="chart-song-details">
                                <span class="chart-song-title">
                                    <h2><a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a></h2>
                                </span>
                                <span class="chart-song-artist">
                                    <h3><?php the_terms( $post->ID, 'artist', ' ', ', ', ' ' ); ?></h3>
                                </span>
                            <span class="chart-album-title">
							<h4><?php $_my_meta_value_key = get_post_meta($post->ID, '_my_meta_value_key', true ); 
                            if($_my_meta_value_key != ""){
                            ?>
                            <?php echo $_my_meta_value_key; ?>
                            <?php } ?></h4>
                            </span>
                            <span class="chart-song-label">
                            <h4><?php the_terms( $post->ID, 'record-label', ' ', '/', ' ' ); ?></h4>
                            </span>
                       </div>
                       <div class="chart-song-links">
						  <?php if(get_field('itunes')) : ?>
                                <a href="<?php the_field('itunes'); ?>" title="Buy <?php the_title(); ?> on iTunes" /><div class="link-itunes"></div></a>
                          <?php endif; ?>
                          <?php if(get_field('amazon')) : ?>
                                <a href="<?php the_field('amazon'); ?>" title="Buy <?php the_title(); ?> on Amazon"><div class="link-amazon"></div></a>
                          <?php endif; ?>
                       </div>
                    </div>

    
</article><!-- #post-## -->
