<?php
/**
 * The template for displaying search results pages.
 *
 * @package Forward
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'ktforward' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php if ( 'charts' == get_post_type() ): ?>
                <div class="post-separator">
                    <div class="container">
                    <?php
                        /* Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        /* edit this!! */
						get_template_part( 'content-full', get_post_type() );
                    ?>
                    </div>
                </div>
                <?php elseif ( 'post' == get_post_type() ): ?>
                <div class="post-separator">
                    <div class="container">
                    <?php
                        /* Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        /* edit this!! */
						get_template_part( 'content', get_post_type() );
                    ?>
                    </div>
                </div>                
                <?php else : ?>
                <div class="post-separator">
                    <div class="container">
                    <?php
                        /* Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'home', get_post_type() );
                    ?>
                    </div>
                </div>
                <?php endif; ?>

			<?php endwhile; ?>

			<?php ktforward_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
