<?php
/**
 * Template Name: Home
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<!-- custom intro section -->       		
			<div>
                    <div class="post-separator intro-bg">
                        <div class="container">
                        	<div class="intro-table">
                                <h2 class="intro-text">Building bridges between
                                <span>music</span>,
                                <span>web development</span>,
                                and <span>social media</span>.</h2>
                            </div>
                            <div class="intro-linkbox">
								<?php wp_nav_menu( array( 'theme_location' => 'intro', 'container_class' => 'intro-menu' ) ); ?>
                            </div>
                        </div>
                    </div> 
            </div>

		<!-- latest project -->       		
			<div>
				<?php $args = array( 'numberposts' => 1, 'post_type' => 'projects' ); 
                    $lastposts = get_posts( $args ); 
                    foreach($lastposts as $post) : setup_postdata($post); 
                ?>
                    <div class="post-separator">
                        <div class="container">
                            <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'home', 'projects' );
                                ?>
                        </div>
                    </div> 
                <?php endforeach; ?>
            </div>

		<!-- latest posts -->       		
			<div>
				<?php $args = array( 'numberposts' => 1 ); 
                    $lastposts = get_posts( $args ); 
                    foreach($lastposts as $post) : setup_postdata($post); 
                ?>
                    <div class="post-separator">
                        <div class="container">
                            <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'content', get_post_format() );
                                ?>
                        </div>
                    </div> 
                <?php endforeach; ?>
            </div>

		<!-- latest review -->       		
			<div class="homeblock-reviews">
				<?php $args = array( 'numberposts' => 1, 'post_type' => 'reviews' ); 
                    $lastposts = get_posts( $args ); 
                    foreach($lastposts as $post) : setup_postdata($post); 
                ?>
			<?php if ( get_field( 'use_white_text' ) ): ?>
                <div class="post-separator review-white-text">
			<?php else: ?>
                <div class="post-separator">
			<?php endif; ?>
                        <div class="container">
                            <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'home', 'reviews' );
                                ?>
                        </div>
                    </div> 
                <?php endforeach; ?>
            </div>
            
		<!-- latest chart -->       		
			<div>
				<?php $args = array( 'numberposts' => 1, 'post_type' => 'charts' ); 
                    $lastposts = get_posts( $args ); 
                    foreach($lastposts as $post) : setup_postdata($post); 
                ?>
                    <div class="post-separator">
                        <div class="container">
                            <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'home', 'charts' );
                                ?>
                        </div>
                    </div> 
                <?php endforeach; ?>
            </div>
                        

			<?php ktforward_paging_nav(); ?>
            
            
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
